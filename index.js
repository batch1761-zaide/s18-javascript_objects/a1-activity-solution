
function Pokemon(name, level){
	//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	//properties
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));

		target.health = (target.health - this.attack)

		if (target.health < 5) {
			target.faint()
		}

	};

	this.faint = function(){
		console.log(this.name + " "+ "fainted. :(")
	}

}

let pikachu = new Pokemon("Pikachu", 16)
let charizard = new Pokemon("Charizard", 20)
let squirtle = new Pokemon("Squirtle",10)
let bulbasaur = new Pokemon("Bulbasaur",10)
let vaporeon = new Pokemon("Vaporeon",17)
let flareon = new Pokemon("Flareon",17)
let sylveon = new Pokemon("Espeon",17)
let umbreon = new Pokemon("Umbreon",17)
let jolteon = new Pokemon("Jolteon",17)
let leafeon = new Pokemon("Leafeon",17)
let glaceon = new Pokemon("Glaceon",17)
let espeon = new Pokemon("Espeon",17)

console.log("Choose your Pokemon");
let arrayPokemons = [pikachu, charizard, squirtle, bulbasaur,vaporeon, flareon, sylveon, umbreon, jolteon, leafeon, glaceon, espeon]

for (let i=0; i<arrayPokemons.length; i++){

	console.log(arrayPokemons[i].name + " - Level: " + arrayPokemons[i].level + " Health: " + arrayPokemons[i].health)

}




//Activity :
/*

	1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
	    - Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	    (target.health - this.attack)

	2.) If health is below 5, invoke faint function.

*/

